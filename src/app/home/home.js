/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
angular.module( 'ngBoilerplate.home', [
  'ui.router',
  'plusOne'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider ) {
  $stateProvider.state( 'home', {
    url: '/home',
    views: {
      "main": {
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'
      }
    },
    data:{ pageTitle: 'Home' }
  });
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'HomeCtrl', function HomeController( $scope ) {

     /*---------owl-carousel--------*/
      $(document).ready(function() {




        $("#Banner").owlCarousel({
          autoPlay: 3000,

          lazyLoad : true,
          navigation : true,
          singleItem : true,
          transitionStyle : "fadeUp"
        });








        //var owl = $("#owl-demo");
        //owl.owlCarousel({
        //  autoPlay: 3000, //Set AutoPlay to 3 seconds
        //  navigation : true,
        //  singleItem : true,
        //  transitionStyle : "fadeUp"
        //});


        $("#Partner").owlCarousel({
          autoPlay: 3000, //Set AutoPlay to 3 seconds
          items : 4,
          lazyLoad : true,
          navigation : true

        });
        $("#NewsEvent").owlCarousel({
          items : 4,
          lazyLoad : true,
          navigation : true
        });
      });
/*--------sliphover------*/
      $(function() {
        $('#container').sliphover({
          target:'img',
          title: 'title',
          backgroundColor:'rgba(33, 150, 169, 0.87)'
        });
      });
 /*---------Banner-------*/
      //$scope.banner=[
      //  {
      //    bannerimg:'assets/images/home/fullimage7.jpg'
      //  },
      //  {
      //    bannerimg:'assets/images/home/fullimage3.jpg'
      //  }
      //];
/*---------Featured-products-------*/
      $scope.fproducts=[
        {
          fproductimg:'assets/images/home/1.jpg',
          fproductheading:'Lorem ipsum dolor sit amet',
          fproductarticle:'Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet'
        },
        {
          fproductimg:'assets/images/home/4.jpg',
          fproductheading:'Lorem ipsum dolor sit amet',
          fproductarticle:'Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet'
        },
        {
          fproductimg:'assets/images/home/2.jpg',
          fproductheading:'Lorem ipsum dolor sit amet',
          fproductarticle:'Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet'
        },
        {
          fproductimg:'assets/images/home/3.jpg',
          fproductheading:'Lorem ipsum dolor sit amet',
          fproductarticle:'Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet'
        }
        //{
        //  fproductimg:'assets/images/home/4.jpg',
        //  fproductheading:'Lorem ipsum dolor sit amet',
        //  fproductarticle:'Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet'
        //},
        //{
        //  fproductimg:'assets/images/home/3.jpg',
        //  fproductheading:'Lorem ipsum dolor sit amet',
        //  fproductarticle:'Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet'
        //},
        //{
        //  fproductimg:'assets/images/home/2.jpg',
        //  fproductheading:'Lorem ipsum dolor sit amet',
        //  fproductarticle:'Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet'
        //},
        //{
        //  fproductimg:'assets/images/home/1.jpg',
        //  fproductheading:'Lorem ipsum dolor sit amet',
        //  fproductarticle:'Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet'
        //}
      ];



/*-------Latest-Products-------*/
      $scope.Pdata=[
        {
          pimg:'assets/images/home/shamoli-filling.jpg'
        }
      ];
      $scope.PPdata=[
        {
          ppimg:'assets/images/home/1.jpg',
          pph:'Lorem ipsum dolor sit amet'
        },
        {
          ppimg:'assets/images/home/2.jpg',
          pph:'Lorem ipsum dolor sit amet'
        },
        {
          ppimg:'assets/images/home/3.jpg',
          pph:'Lorem ipsum dolor sit amet'
        },
        {
          ppimg:'assets/images/home/4.jpg',
          pph:'Lorem ipsum dolor sit amet'
        },
        {
          ppimg:'assets/images/home/2.jpg',
          pph:'Lorem ipsum dolor sit amet'
        },
        {
          ppimg:'assets/images/home/1.jpg',
          pph:'Lorem ipsum dolor sit amet'
        },
        {
          ppimg:'assets/images/home/3.jpg',
          pph:'Lorem ipsum dolor sit amet'
        }
      ];
/*---------Services------*/
      $scope.Sdata=[
        {
          si:'fa fa-gear',
          sh:'Annual Maintenance',
          sp:'Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet'
        },
        {
          si:'fa fa-bus',
          sh:'Annual Maintenance',
          sp:'Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet'
        },
        {
          si:'fa fa-car',
          sh:'Annual Maintenance',
          sp:'Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet'
        },
        {
          si:'fa fa-cogs',
          sh:'Annual Maintenance',
          sp:'Lorem ipsum dolor sit ametLorem ipsum dolor sit ametLorem ipsum dolor sit amet'
        }
      ];

});

