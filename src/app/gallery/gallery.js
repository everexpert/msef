angular.module( 'ngBoilerplate.gallery', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'gallery', {
            url: '/gallery',
            views: {
                "main": {
                    controller: 'GalleryCtrl',
                    templateUrl: 'gallery/gallery.tpl.html'
                }
            },
            data:{ pageTitle: 'gallery' }
        });
    })

    .controller( 'GalleryCtrl', function GalleryCtrl( $scope ) {




        $(document).ready(function() {
            $(".fancybox").fancybox({
                openEffect	: 'none',
                closeEffect	: 'none'
            });
        });

        $scope.mjhGallery=[
            {
                mjhbgimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_b.jpg',
                mjhsmimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_m.jpg'
            },
            {
                mjhbgimg:'http://farm6.staticflickr.com/5444/17679973232_568353a624_b.jpg',
                mjhsmimg:'http://farm6.staticflickr.com/5444/17679973232_568353a624_m.jpg'
            },
            {
                mjhbgimg:'http://farm8.staticflickr.com/7367/16426879675_e32ac817a8_b.jpg',
                mjhsmimg:'http://farm8.staticflickr.com/7367/16426879675_e32ac817a8_m.jpg'
            },
            {
                mjhbgimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_b.jpg',
                mjhsmimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_m.jpg'
            },
            {
                mjhbgimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_b.jpg',
                mjhsmimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_m.jpg'
            },
            {
                mjhbgimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_b.jpg',
                mjhsmimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_m.jpg'
            },
            {
                mjhbgimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_b.jpg',
                mjhsmimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_m.jpg'
            },
            {
                mjhbgimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_b.jpg',
                mjhsmimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_m.jpg'
            },
            {
                mjhbgimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_b.jpg',
                mjhsmimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_m.jpg'
            },
            {
                mjhbgimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_b.jpg',
                mjhsmimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_m.jpg'
            },
            {
                mjhbgimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_b.jpg',
                mjhsmimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_m.jpg'
            },
            {
                mjhbgimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_b.jpg',
                mjhsmimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_m.jpg'
            },
            {
                mjhbgimg:'http://farm6.staticflickr.com/5444/17679973232_568353a624_b.jpg',
                mjhsmimg:'http://farm6.staticflickr.com/5444/17679973232_568353a624_m.jpg'
            },
            {
                mjhbgimg:'http://farm8.staticflickr.com/7367/16426879675_e32ac817a8_b.jpg',
                mjhsmimg:'http://farm8.staticflickr.com/7367/16426879675_e32ac817a8_m.jpg'
            },
            {
                mjhbgimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_b.jpg',
                mjhsmimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_m.jpg'
            },
            {
                mjhbgimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_b.jpg',
                mjhsmimg:'http://farm1.staticflickr.com/313/19831416459_5ddd26103e_m.jpg'
            }
        ];



    });
