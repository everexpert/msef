angular.module( 'ngBoilerplate.service', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'service', {
            url: '/service',
            views: {
                "main": {
                    controller: 'ServiceCtrl',
                    templateUrl: 'service/service.tpl.html'
                }
            },
            data:{ pageTitle: 'service' }
        });
    })

    .controller( 'ServiceCtrl', function ServiceCtrl( $scope ) {
        // This is simple a demo for UI Boostrap.
        $scope.dropdownDemoItems = [
            "The first choice!",
            "And another choice for you.",
            "but wait! A third!"
        ];
    })

;
