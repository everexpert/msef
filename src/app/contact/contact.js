app=angular.module( 'ngBoilerplate.contact', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'contact', {
            url: '/contact',
            views: {
                "main": {
                    controller: 'ContactCtrl',
                    templateUrl: 'contact/contact.tpl.html'
                }
            },
            data:{ pageTitle: 'contact' }
        });
    });



app.value("rndAddToLatLon",function ($scope) {
    return Math.floor(((Math.random() < 0.5 ? -1 : 1) * 2) + 1);
})

    .controller( 'ContactCtrl', function ContactCtrl( $scope, $timeout, $log, $http, rndAddToLatLon ) {


        // Enable the new Google Maps visuals until it gets enabled by default.
        // See http://googlegeodevelopers.blogspot.ca/2013/05/a-fresh-new-look-for-maps-api-for-all.html
        google.maps.visualRefresh = true;

        var versionUrl = window.location.host === "rawgithub.com" ? "http://rawgithub.com/nlaplante/angular-google-maps/master/package.json" : "";

        $http.get(versionUrl).success(function (data) {
            if (!data){
                console.error("no version object found!!");
                $scope.version = data.version;
            }
        });

        angular.extend($scope, {
            map: {
                control: {},
                center: {
                    latitude:23,
                    longitude: 90
                },
                options: {
                    streetViewControl: false,
                    panControl: false,
                    maxZoom: 20,
                    minZoom: 3
                },
                zoom: 3,
                dragging: false,
                bounds: {},

                dynamicMarkers: [],
                refresh: function () {
                    $scope.map.control.refresh(origCenter);
                }
            }
        });

        $scope.markersEvents = {
            click: function (gMarker, eventName, model) {
                if(model.$id){
                    model = model.coords;//use scope portion then
                }
                alert("Model: event:" + eventName + " " + JSON.stringify(model));
            }
        };

        $scope.refreshMap = function () {
            //optional param if you want to refresh you can pass null undefined or false or empty arg
            $scope.map.control.refresh({latitude: 0, longitude: 90});
            $scope.map.control.getGMap().setZoom(11);
        };
        $scope.getMapInstance = function () {
            alert("You have Map Instance of" + $scope.map.control.getGMap().toString());
        };

        var markerToClose = null;

        $scope.onMarkerClicked = function (marker) {
//    if (markerToClose) {
//      markerToClose.showWindow = false;
//    }
            markerToClose = marker; // for next go around
            marker.showWindow = true;
            $scope.$apply();
            //window.alert("Marker: lat: " + marker.latitude + ", lon: " + marker.longitude + " clicked!!")
        };

        $scope.onInsideWindowClick = function () {
            alert("Window hit!");
        };

        $timeout(function () {
            var dynamicMarkers = [
                {   id: 1,
                    latitude: 23,
                    longitude: 90
                },
                {
                    id: 2,
                    icon: 'assets/images/plane.png',
                    latitude: 0,
                    longitude: 90
                }
            ];
            _.each(dynamicMarkers, function (marker) {
                marker.closeClick = function () {
                    marker.showWindow = false;
                    $scope.$apply();
                };
                marker.onClicked = function () {
                    $scope.onMarkerClicked(marker);
                };
            });
            $scope.map.dynamicMarkers = dynamicMarkers;
        });

        var origCenter = {latitude: $scope.map.center.latitude, longitude: $scope.map.center.longitude};






    });
