angular.module( 'ngBoilerplate.product', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'product', {
            url: '/product',
            views: {
                "main": {
                    controller: 'ProductCtrl',
                    templateUrl: 'product/product.tpl.html'
                }
            },
            data:{ pageTitle: 'product' }
        });
    })

    .controller( 'ProductCtrl', function ProductCtrl( $scope ) {

        $scope.ProductData=[
            {
                Productimg:'assets/images/home/2.jpg',
                Producth:'Lorem ipsum dolor',
                Productn:'Lorem ipsum',
                Productp:'30$'
            },
            {
                Productimg:'assets/images/home/2.jpg',
                Producth:'Lorem ipsum dolor',
                Productn:'Lorem ipsum',
                Productp:'30$'
            },
            {
                Productimg:'assets/images/home/2.jpg',
                Producth:'Lorem ipsum dolor',
                Productn:'Lorem ipsum',
                Productp:'30$'
            },
            {
                Productimg:'assets/images/home/2.jpg',
                Producth:'Lorem ipsum dolor',
                Productn:'Lorem ipsum',
                Productp:'30$'
            },
            {
                Productimg:'assets/images/home/2.jpg',
                Producth:'Lorem ipsum dolor',
                Productn:'Lorem ipsum',
                Productp:'30$'
            },
            {
                Productimg:'assets/images/home/2.jpg',
                Producth:'Lorem ipsum dolor',
                Productn:'Lorem ipsum',
                Productp:'30$'
            },
            {
                Productimg:'assets/images/home/2.jpg',
                Producth:'Lorem ipsum dolor',
                Productn:'Lorem ipsum',
                Productp:'30$'
            },
            {
                Productimg:'assets/images/home/2.jpg',
                Producth:'Lorem ipsum dolor',
                Productn:'Lorem ipsum',
                Productp:'30$'
            },
            {
                Productimg:'assets/images/home/2.jpg',
                Producth:'Lorem ipsum dolor',
                Productn:'Lorem ipsum',
                Productp:'30$'
            },
            {
                Productimg:'assets/images/home/2.jpg',
                Producth:'Lorem ipsum dolor',
                Productn:'Lorem ipsum',
                Productp:'30$'
            },
            {
                Productimg:'assets/images/home/2.jpg',
                Producth:'Lorem ipsum dolor',
                Productn:'Lorem ipsum',
                Productp:'30$'
            },
            {
                Productimg:'assets/images/home/2.jpg',
                Producth:'Lorem ipsum dolor',
                Productn:'Lorem ipsum',
                Productp:'30$'
            }

        ];

    });
